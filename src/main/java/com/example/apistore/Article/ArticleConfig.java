//package com.example.apistore.Article;
//
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import java.util.List;
//
//@Configuration
//public class ArticleConfig {
//
//    @Bean
//    CommandLineRunner commandLineRunner(ArticleRepository repository){
//        return args -> {
//            Article book = new Article(
//                    "Harry Potter",
//                    23.21F,
//                    1);
//            Article narnia = new Article(
//                    "Chronicles of Narnia",
//                    23F,
//                    12);
//
//            repository.saveAll(
//                    List.of(book, narnia)
//            );
//        };
//    }
//}
