package com.example.apistore.Article;

import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.lang.reflect.Field;
import java.util.List;

@Service
public class ArticleService {

    public ArticleService(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    private final ArticleRepository articleRepository;

    public List<Article> getArticles() {
        return articleRepository.findAll();
    }

    public Article getArticle(Long id){
        return articleRepository.findById(id).get();
    }

    public void addNewArticle(Article article) {
        articleRepository.save(article);
    }

    public void deleteArticle(Long articleId) {
        boolean exists = articleRepository.existsById(articleId);
        if (!exists) {
            throw new IllegalStateException(
                    "Article with id " + articleId + "does not exists"
            );
        }
        articleRepository.deleteById(articleId);
    }

    @Transactional
    public void updateArticle(Long articleId, Article article) {
        Article articleToUpdate = articleRepository.findById(articleId)
                .orElseThrow(
                        () -> new IllegalStateException(
                                "Article with id " + articleId + "does not exists"
                        )
                );
        articleToUpdate.setName(article.getName());
        articleToUpdate.setPrice(article.getPrice());
        articleToUpdate.setQuantity(article.getQuantity());
    }
    @Transactional
    public void patchArticle(Long articleId, Article article) {
        Article articleToUpdate = articleRepository.findById(articleId)
                .orElseThrow(
                        () -> new IllegalStateException(
                                "Article with id " + articleId + "does not exists"
                        )
                );
        if (article.getName() != null)
            articleToUpdate.setName(article.getName());
        if (article.getPrice() != null)
            articleToUpdate.setPrice(article.getPrice());
        if (article.getQuantity() != null)
            articleToUpdate.setQuantity(article.getQuantity());
    }
}

