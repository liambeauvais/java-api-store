package com.example.apistore.order;

import com.example.apistore.Article.Article;
import jakarta.persistence.*;

@Entity
@Table
public class OrderArticle {
    @Id
    @SequenceGenerator(
            name = "order_article_sequence",
            sequenceName = "order_article_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "order_article_sequence"
    )
    private Long id;

    private Integer quantity;

    @ManyToOne
    @JoinColumn(
            name = "article_id",
            referencedColumnName = "id"
    )
    private Article article;

    @ManyToOne
    @JoinColumn(
            name = "order_id",
            referencedColumnName = "id"
    )
    private Order order;

    public OrderArticle() {
    }

    public OrderArticle(Integer quantity, Article article, Order order) {
        this.quantity = quantity;
        this.article = article;
        this.order = order;
    }
}
