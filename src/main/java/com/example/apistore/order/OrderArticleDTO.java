package com.example.apistore.order;

public record OrderArticleDTO (Long id, Integer quantity, Long articleId){
}
