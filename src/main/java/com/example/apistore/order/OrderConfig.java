//package com.example.apistore.order;
//
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import java.util.List;
//
//@Configuration
//public class OrderConfig {
//
//    @Bean
//    CommandLineRunner OrderLineRunner(OrderRepository repository) {
//        return args -> {
//            Order book = new Order(
//                    "test"
//            );
//            Order narnia = new Order(
//                    "Chronicles of Narnia"
//            );
//
//            repository.saveAll(
//                    List.of(book, narnia)
//            );
//        };
//    }
//}
