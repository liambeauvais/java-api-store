package com.example.apistore.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/v1/orders")
public class OrderController {

    private final OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }


    @GetMapping
    public List<Order> getOrders() {
        return orderService.getOrders();
    }

    @PostMapping
    public void createOrder(@RequestBody OrderDTO order) {
        orderService.addNewOrder(order);
    }

    @DeleteMapping(path = "{articleId}")
    public void deleteOrder(@PathVariable("articleId") Long articleId) {
        orderService.deleteOrder(articleId);
    }

    @PutMapping(path = "{articleId}")
    public void updateOrder(
            @PathVariable("articleId") Long articleId,
            @RequestBody Order order) {
        orderService.updateOrder(articleId,order);
    }

    @PatchMapping(path = "{articleId}")
    public void patchOrder(
            @PathVariable("articleId") Long articleId,
            @RequestBody Order order
    ){
        orderService.patchArticle(articleId,order);
    }
}
