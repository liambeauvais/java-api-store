package com.example.apistore.order;

import java.util.List;

public record OrderDTO (Long id, String name, OrderArticleDTO[] orderList) {
}
