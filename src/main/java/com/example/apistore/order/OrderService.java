package com.example.apistore.order;

import com.example.apistore.Article.Article;
import com.example.apistore.Article.ArticleRepository;
import com.example.apistore.Article.ArticleService;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderService {
    public OrderService(OrderRepository orderRepository, ArticleRepository articleRepository) {
        this.orderRepository = orderRepository;
        this.articleRepository = articleRepository;
    }

    private final OrderRepository orderRepository;
    private final ArticleRepository articleRepository;

    public List<Order> getOrders() {
        return orderRepository.findAll();
    }

    public void addNewOrder(OrderDTO orderDTO) {
        Order order = new Order(
                orderDTO.name()
        );
        order = orderRepository.save(order);
        List<OrderArticle> orderArticleList = new ArrayList<>();
        for (OrderArticleDTO orderArticleDTO:
                orderDTO.orderList()) {
            ArticleService service  = new ArticleService(articleRepository);
            Article article = service.getArticle(orderArticleDTO.articleId());
            OrderArticle orderArticle = new OrderArticle(
                    orderArticleDTO.quantity(),
                    article,
                    order
            );
            orderArticleList.add(orderArticle);
        }
        order.setOrderArticles(orderArticleList);
        orderRepository.save(order);
    }

    public void deleteOrder(Long articleId) {
        boolean exists = orderRepository.existsById(articleId);
        if (!exists) {
            throw new IllegalStateException(
                    "Article with id " + articleId + "does not exists"
            );
        }
        orderRepository.deleteById(articleId);
    }

    @Transactional
    public void updateOrder(Long articleId, Order order) {
        Order orderToUpdate = orderRepository.findById(articleId)
                .orElseThrow(
                        () -> new IllegalStateException(
                                "Article with id " + articleId + "does not exists"
                        )
                );
        orderToUpdate.setName(order.getName());
    }
    @Transactional
    public void patchArticle(Long articleId, Order order) {
        Order orderToUpdate = orderRepository.findById(articleId)
                .orElseThrow(
                        () -> new IllegalStateException(
                                "Article with id " + articleId + "does not exists"
                        )
                );
        if (order.getName() != null)
            orderToUpdate.setName(order.getName());
        }
}

